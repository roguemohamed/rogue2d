﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour {

	public float moveTime = 0.1f;
	public LayerMask blockingLayer;

	private float movementSpeed;
	private BoxCollider2D boxCollider;
	private Rigidbody2D rb2D;


	protected virtual void Awake(){

		boxCollider = GetComponent<BoxCollider2D> ();
		rb2D = GetComponent<Rigidbody2D> ();
	
	}

	protected virtual void Start () {

		movementSpeed = 1f / moveTime;
		
	}


	protected IEnumerator SmoothMovement(Vector2 end){
		//calcular a que distancia estamos del punto final
		float remaningDistance = Vector2.Distance(rb2D.position, end);
		//se usa float.Epsilon para avanzar sin disminuir velocidad
		while (remaningDistance > float.Epsilon) {
			Vector2 newPosition = Vector2.MoveTowards (rb2D.position, end, movementSpeed * Time.deltaTime);
			rb2D.MovePosition (newPosition);
			//volver a calcular la distancia para seguir avanzando hasta que llegeuemos
			remaningDistance = Vector2.Distance (rb2D.position, end);
			yield return null;
		}
	}


	//movimiento de personajes
	protected bool Move(int xDir, int yDir, out RaycastHit2D hit){
	
		Vector2 start = transform.position;
		Vector2 end = start + new Vector2 (xDir, yDir); //suma de vectores, (0,0) + (1,0) = (1, 0)
		boxCollider.enabled = false;
		hit = Physics2D.Linecast (start, end, blockingLayer);
		boxCollider.enabled = true;
		//ni no nos hemos encontrado ningun objeto destruible
		if (hit.transform == null) {

			//hacer el movimiento
			StartCoroutine(SmoothMovement(end));
			return true;
		}
		return false;

	}
		

	//metodo abstracto para usar en el movimiento tanto del jugador como los enemigos
	protected abstract void OnCantMove (GameObject go);



	//modificador virtual sirve para modificar luego el metodo, es decir, hacer un override
	protected virtual bool AttemptMove(int xDir, int yDir){

		RaycastHit2D hit;
		bool canMove = Move (xDir, yDir, out hit);

		if (!canMove) {

			//Si es un muro, el jugador dañara al muro.
			//Si es un enemigo, haremos que le quite puntos de vida al jugador
			OnCantMove (hit.transform.gameObject);
		}


		return canMove;


	}
}
