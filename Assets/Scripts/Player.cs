﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject {


	public AudioClip moveSound1, moveSound2, eatSound1, eatSound2, drinkSound1, drinkSound2, gameOverSound;


	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public float restartLevelDelay = 1f;
	public Text foodText;

	private Animator animator;
	private int food;
	private Vector2 touchOrigin = -Vector2.one;


	protected override void Awake(){

		animator = GetComponent<Animator> ();
		base.Awake ();

	}

	protected override void Start(){
		
		food = GameManager.instance.playerFoodPoints;
		foodText.text = "Food " + food;
		base.Start ();

	}

	void OnDisable(){

		GameManager.instance.playerFoodPoints = food;

	}

	void ChechIfGameOver(){

		if (food <= 0) {
			SoundManager.instance.PlaySingle (gameOverSound);
			SoundManager.instance.musicSource.Stop ();
			GameManager.instance.GameOver ();
			food = 100;	
		}
	}

	protected override bool AttemptMove (int xDir, int yDir)
	{
		food--;
		foodText.text = "Food " + food;
		bool canMove = base.AttemptMove (xDir, yDir);
		if(canMove) SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
		ChechIfGameOver ();
		GameManager.instance.playersTurn = false;

		return canMove;
	}



	//mover personaje
	void Update(){
	
		if(!GameManager.instance.playersTurn || GameManager.instance.doingSetup)
			return;

		int horitzontal = 0;
		int vertical = 0;
#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
		horitzontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		moveSide(horitzontal);

		if (horitzontal != 0)
			vertical =  0;
#else
		if(Input.touchCount > 0){
			Touch myTouch = Input.touches[0];
			
			if(myTouch.phase == TouchPhase.Began){
				touchOrigin = myTouch.position;
			}

			else if (myTouch.phase == TouchPhase.Ended && touchOrigin!=-Vector2.one){

					Vector2 touchEnd = myTouch.position;
					float x = touchEnd.x - touchOrigin.x;
					float y = touchEnd.y - touchOrigin.y;
					moveSide((int)x);
						if(x != 0 || y !=0){
									if(Mathf.Abs(x) > Mathf.Abs(y)){
										
										horitzontal = x > 0 ? 1: -1;

									}else{
										vertical = x > 0 ? 1: -1;
									}
						}		
				}
		}
#endif
		if (horitzontal != 0 || vertical != 0)
			AttemptMove (horitzontal, vertical);

		}

	

	//implementacion para el jugador
	protected override void OnCantMove(GameObject go){

		Wall hitwall = go.GetComponent<Wall> ();
		if (hitwall != null) {
			hitwall.DamageWall (wallDamage);
			animator.SetTrigger ("playerChop");
		}
	}

	//Recargar escena
	void Restart(){
		
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void LoseFood(int loss){
		food -= loss;
		foodText.text = "-" + loss + " Food " + food;
		animator.SetTrigger ("playerHit");
		ChechIfGameOver ();
	}

	private void OnTriggerEnter2D(Collider2D other){
		//si llega a la salida, sale del juego
		if (other.CompareTag ("Exit")) {
			
			Invoke ("Restart", restartLevelDelay);
			enabled = false;

			//Encuentra comida, suben puntos de vida, y desaparece el alimento
		} else if (other.CompareTag ("Food")) {
			
			food += pointsPerFood;
			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);
			foodText.text = "+" + pointsPerFood + " Food " + food;
			other.gameObject.SetActive (false);

		} else if (other.CompareTag ("Soda")) {
			
			food += pointsPerSoda;
			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);
			foodText.text = "+" + pointsPerSoda + " Food " + food;
			other.gameObject.SetActive (false);
		}


	}


	//Metodo que cambia el sentido del personaje dependiendo del sentido horizontal al que se dirije
	public void moveSide(int horitzontal){

		if(horitzontal>0 && transform.localScale==new Vector3(-1,1,1)){
			transform.localScale += new Vector3(2,0,0);
		} else if (horitzontal<0 && transform.localScale==new Vector3(1,1,1)){
			transform.localScale += new Vector3(-2,0,0);

		}
	}

}
