﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public float turnDelay = 0.1f;
	public float levelStartDelay = 2f;
	public bool doingSetup;
	public BoardManager boardScript;
	public int playerFoodPoints = 100;
	[HideInInspector]public bool playersTurn = true;



	private List<Enemy> enemies = new List<Enemy>();
	private bool enemiesMoving;
	private int level = 0;
	private GameObject levelImage;
	private Text levelText;
	private Button buttonPlay;



	private void Awake(){

		if (GameManager.instance == null) {

			GameManager.instance = this;

		} else if (GameManager.instance != this) {
		
			Destroy (gameObject);
			
		}

		DontDestroyOnLoad (gameObject);

		boardScript = GetComponent<BoardManager> ();
	}
		


	void InitGame(){
		//setear texto en pantalla
		doingSetup = true;
		levelImage = GameObject.Find ("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text> ();
		levelText.text = "Dia " + level;
		buttonPlay = GameObject.Find ("buttonPlay").GetComponent<Button> ();
		buttonPlay.gameObject.SetActive (false);
		levelImage.SetActive (true);

		enemies.Clear ();
		boardScript.SetupScene(level);

		//se ejcutara HideLevelImage duranto tantos segundos dados por la variable levelStartDelay
		Invoke ("HideLevelImageAndButtonPlay", levelStartDelay);



	}

	private void HideLevelImageAndButtonPlay(){
		buttonPlay.gameObject.SetActive (false);
		levelImage.SetActive (false);
		doingSetup = false;

	
	}


	public void GameOver(){
		enabled = false;
		levelText.text = "Despues de " + level + " dias, has muerto.";
		buttonPlay.gameObject.SetActive (true);
		levelImage.SetActive (true);
	}



	IEnumerator MoveEnemies(){

		enemiesMoving = true;

		yield return new WaitForSeconds(turnDelay);

		if (enemies.Count == 0) {

			yield return new WaitForSeconds (turnDelay);
		}


		for (int i = 0; i < enemies.Count; i++) {

			enemies [i].MoveEnemy ();
			yield return new WaitForSeconds (enemies[i].moveTime);
		}

		playersTurn = true;

		enemiesMoving = false;
	}



	private void Update(){

		if (playersTurn || enemiesMoving || doingSetup)
			return;
		StartCoroutine (MoveEnemies ());


	}

	public void onClickedGameOver (){
		//enabled = true;
		level = 0;
		levelImage.SetActive (false);
		SceneManager.LoadScene ("main");
		enabled = true;
	}

	public void AddEnemyToList(Enemy enemy){
		enemies.Add (enemy);
	}


	//Metodos que se llaman cuando se recarga una escena
	private void OnEnable(){
		SceneManager.sceneLoaded += OnLevelFinishedLoading;

	}

	private void OnDisable(){
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode ){

		level++;
		InitGame ();

	}

}
