﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioSource musicSource;
	public AudioSource sfxSource;


	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;

	public static SoundManager instance;


	//Singletone para SoundManager
	//Una clase singletone es una clase que se puede instanciar solo una vez
	private void Awake(){
		//si la instancia de esta clase no existe, se hace
		if (SoundManager.instance == null) {

			SoundManager.instance = this;

		} else if (SoundManager.instance != this) {

			Destroy (gameObject);

		}

		DontDestroyOnLoad (gameObject);
	}

	public void PlaySingle(AudioClip clip ){
		sfxSource.pitch = 1f;
		sfxSource.clip = clip;
		sfxSource.Play ();
	}


	public void RandomizeSfx(params AudioClip[] clips){

		int randomIndex = Random.Range (0, clips.Length);
		float randomPitch = Random.Range (lowPitchRange, highPitchRange);
		sfxSource.pitch = randomPitch;
		sfxSource.clip = clips [randomIndex];
		sfxSource.Play ();


	}


}
