﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Enemy : MovingObject {

	public AudioClip enemyAtack1, enemyAtack2;

	public int playerDamage;

	private Animator animator;
	private Transform target;
	private bool skipMove;


	protected override void Awake(){

		animator = GetComponent<Animator> ();
		base.Awake ();

	}



	protected override void Start(){
		
		GameManager.instance.AddEnemyToList (this);
		animator = GetComponent<Animator> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		base.Start ();
	
	}


	protected override bool AttemptMove (int xDir, int yDir)
	{
		if (skipMove) {
			skipMove = false;
			return false;
		}
		bool canMove = base.AttemptMove (xDir, yDir);
		skipMove = true;

		return canMove;
	}


	public void MoveEnemy(){
		int xDir = 0;
		int yDir = 0;
		//si el enemigo y el jugador estan cerca en el eje vertical (eje x)
		if (Math.Abs (target.position.x - transform.position.x) < float.Epsilon) {
			//el enemigo se mueve hacia el jugador tendiendo a la posicion y del juador
			//Tando en diagonal como en horizontal y en vertical
			yDir = target.position.y > transform.position.y ? 1 : -1;
			xDir = UnityEngine.Random.Range (-1,1);
		} else {
			xDir = target.position.x > transform.position.x ? 1 : -1;
			yDir = UnityEngine.Random.Range (-1,1);
		}
		AttemptMove (xDir, yDir);
	}


	protected override void OnCantMove (GameObject go)
	{
		Player hitPlayer = go.GetComponent<Player> ();
		if (hitPlayer != null) {

			hitPlayer.LoseFood (playerDamage);
			animator.SetTrigger("enemyAttack");
			SoundManager.instance.RandomizeSfx (enemyAtack1, enemyAtack2);

		}
	}
		
}
