﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

	public int columns = 8;
	public int rows = 8;

	public GameObject[] floorTiles, outerWallTiles, wallTiles, foodTiles, enemyTiles, exit ;  //Losetas

	// Exit tambien sera un array porque se ha usado salidas con sprites distintos Salida con diseño aleatorio

	private Transform BoardHolder;

	private List<Vector2> girdPositions = new List<Vector2>();


	//inicializar lista para componentes interactuables
	void InitializeList(){
		
		girdPositions.Clear ();
		for (int x = 1; x < columns - 1; x++) {

			for (int y = 1; y < rows - 1; y++) {
				
				girdPositions.Add (new Vector2 (x, y));

			}

		}

	}

	Vector2 randomPosition(){
	
		int randomIndex = Random.Range (0, girdPositions.Count);
		Vector2 randomPositions = girdPositions[randomIndex];
		girdPositions.RemoveAt (randomIndex);
		return randomPositions;

	}


	//Crear pared, comida
	void LayoutObjectAtRandom(GameObject[] tileArray, int min, int max)
	{
	
		int objectCount = Random.Range (min, max+1);
		for (int i = 0; i < objectCount; i++) {

			Vector2 randoPosition = randomPosition ();
			GameObject titleChoice = GetRandomInArray (tileArray);
			GameObject instance = Instantiate (titleChoice, randoPosition, Quaternion.identity);
			instance.transform.SetParent (BoardHolder);


		}

	}


	public void SetupScene(int level)
	{

		BoardSetup ();
		InitializeList ();
		LayoutObjectAtRandom (wallTiles, 5, 9);
		LayoutObjectAtRandom (foodTiles, 1, 5);
		//Numero de enemigos generados a partir de la dificultad del nivel aplicandole una función logaritmica en base 2
		int enemyCount = (int)Mathf.Log (level, 2);
		LayoutObjectAtRandom (enemyTiles, enemyCount, enemyCount);
		Instantiate (GetRandomInArray(exit), new Vector2(columns-1, rows-1), Quaternion.identity );

	}

	//Crear escenario
	void BoardSetup()
	{
		BoardHolder= new GameObject ("Board").transform;
		for(int x = -1; x < columns + 1; x++)
		{
			
			for(int y = -1; y < rows + 1; y++)
			{

				GameObject toInstantiate = GetRandomInArray (floorTiles);

				if (x == -1 || y == -1 || x == columns || y ==	rows) 
				{

					toInstantiate = GetRandomInArray (outerWallTiles);

				}

				GameObject instance = Instantiate (toInstantiate, new Vector2 (x, y) , Quaternion.identity);
				instance.transform.SetParent (BoardHolder);
			}
				
		}

	}


	GameObject GetRandomInArray(GameObject[] array)
	{

		return array [Random.Range (0, array.Length)];

	}


}
