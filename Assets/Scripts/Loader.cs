﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

	public GameObject gameManager;

	private void Awake(){

		if (GameManager.instance == null) {
		
			Instantiate (gameManager);
		
		}

	}

	public void onClickGameOver(){
		GameManager.instance.playerFoodPoints = 100;
		GameManager.instance.onClickedGameOver ();
	}
}
